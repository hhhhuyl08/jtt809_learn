package com.sjx.jtt809.client.pojo;

import com.sjx.jtt809.server.pojo.BasePackage;
import com.sjx.jtt809.server.util.ConstantJtt809Util;
import io.netty.buffer.ByteBuf;

/**
 * 主链路连接保持请求消息
 * 链路类型:主链路。
 * 消息方向:下级平台往上级平台。
 * 业务数据类型标识:UP_ LINK 下 EST_ REQ。
 * 描述:下级平台向上级平台发送主链路连接保持清求消息，以保持主链路的连接。
 * 主链路连接保持清求消息，数据体为空。
 */
public class RequestClientJtt809_0x1005 extends BasePackage {

    public RequestClientJtt809_0x1005() {
        super(ConstantJtt809Util.UP_LINKETEST_REQ);
        this.msgBodyLength = 0;
    }

    @Override
    protected void encodeImpl(ByteBuf buf) {

    }
}
