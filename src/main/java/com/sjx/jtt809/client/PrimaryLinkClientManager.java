package com.sjx.jtt809.client;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.sjx.jtt809.client.handler.initializer.ClientJtt809Initializer;
import com.sjx.jtt809.client.pojo.RequestClientJtt809_0x1001;
import com.sjx.jtt809.client.pojo.RequestJtt809_0x1200_VehicleColor;
import com.sjx.jtt809.client.pojo.RequestJtt809_0x1202;
import com.sjx.jtt809.client.pojo.ResponseClientJtt809_0x1002;
import com.sjx.util.MockUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * 主链路
 * 下级平台客户端
 */
public class PrimaryLinkClientManager implements Runnable {
    private static final Log logger = LogFactory.get();

    private Bootstrap bootstrap;

    private Channel channel;

    private String ip;

    private int port;

    private String downLinkIp;

    private int downLinkPort;

    public PrimaryLinkClientManager(String ip, int port, String downLinkIp, int downLinkPort) {
        this.ip = ip;
        this.port = port;
        this.downLinkIp = downLinkIp;
        this.downLinkPort = downLinkPort;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDownLinkIp() {
        return downLinkIp;
    }

    public void setDownLinkIp(String downLinkIp) {
        this.downLinkIp = downLinkIp;
    }

    public int getDownLinkPort() {
        return downLinkPort;
    }

    public void setDownLinkPort(int downLinkPort) {
        this.downLinkPort = downLinkPort;
    }

    public void start() {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        try {
            bootstrap = new Bootstrap();
            bootstrap.group(eventLoopGroup)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .channel(NioSocketChannel.class)
                    .handler(new ClientJtt809Initializer(ip, port, downLinkIp, downLinkPort));

            // 连接服务端
            ChannelFuture channelFuture = bootstrap.connect(ip, port);
            channelFuture.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (!future.isSuccess()) {
                        final EventLoop loop = future.channel().eventLoop();
                        loop.schedule(new Runnable() {

                            @Override
                            public void run() {
                                logger.info("======> 上级平台连接不上，开始重连操作......");
                                start();
                            }
                        }, 5L, TimeUnit.SECONDS);
                    } else {
                        channel = future.channel();
                        logger.info("======> 上级平台连接成功......");
                    }
                }
            });

            Thread.sleep(5000);

            // 发送登录指令
            RequestClientJtt809_0x1001 jtt8090X1001Client = new RequestClientJtt809_0x1001();
            jtt8090X1001Client.setEncryptFlag((short) 0);
            jtt8090X1001Client.setEncryptKey(0);

            jtt8090X1001Client.setUserId(10023);
            jtt8090X1001Client.setPassword("2019test");
            jtt8090X1001Client.setDownLinkIp(this.downLinkIp);
            jtt8090X1001Client.setDownLinkPort(this.downLinkPort);
            channel.writeAndFlush(jtt8090X1001Client.encode());

            Thread.sleep(3000);

            if (ResponseClientJtt809_0x1002.isIsLoginFlagFromUpPlatform()) {

                long reqCount = 0;
                while (true) {

                    RequestJtt809_0x1202 requestJtt8090x1202 = new RequestJtt809_0x1202();
                    requestJtt8090x1202.setVehicleNo(MockUtil.getVechileNo());
                    requestJtt8090x1202.setVehicleColor(RequestJtt809_0x1200_VehicleColor.BLUE);
                    requestJtt8090x1202.setEncryptFlag((short) 0);
                    requestJtt8090x1202.setEncryptKey((short) 0);
                    requestJtt8090x1202.setEncrypt((byte) 0);
                    requestJtt8090x1202.setDateTime(Calendar.getInstance());

                    requestJtt8090x1202.setLon(MockUtil.randomLonLat(116.891056, 118.145179, 23.565182, 25.212862, "lon"));
                    requestJtt8090x1202.setLat(MockUtil.randomLonLat(116.891056, 118.145179, 23.565182, 25.212862,  "lat"));
                    requestJtt8090x1202.setVec1(Convert.toShort(60));
                    requestJtt8090x1202.setVec2(Convert.toShort(60));
                    requestJtt8090x1202.setVec3((short) RandomUtil.randomInt(200));
                    requestJtt8090x1202.setDirection((short) RandomUtil.randomInt(0, 360));
                    requestJtt8090x1202.setAltitude((short) RandomUtil.randomInt(0, 6000));

                    channel.writeAndFlush(requestJtt8090x1202);

                    reqCount++;

                    logger.info("======================> reqCount = {}", reqCount);

                    // 等待多久在发送
                    Thread.sleep(10000);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        start();
    }
}
