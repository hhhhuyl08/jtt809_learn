package com.sjx.jtt809.server.pojo.command;

import com.sjx.jtt809.server.pojo.BasePackage;
import com.sjx.jtt809.server.util.ConstantJtt809Util;
import io.netty.buffer.ByteBuf;

/**
 * 主链路登录应答消息
 * 链路类型:主链路。
 * 消息方向:上级平台往下级平台。
 * 业务数据类型标识:UP_CONNCCT_RSP。
 * 描述:上级平台对下级平台登录请求信息、进行安全验证后，返回相应的验证结果。
 */
public class RequestJtt809_0x1002 extends BasePackage {

    /**
     * 验证结果，定义如下：
     * 0x00:成功;
     * 0x01:IP 地址不正确；
     * 0x02:接入码不正确；
     * 0x03:用户没用注册；
     * 0x04:密码错误；
     * 0x05:资源紧张，稍后再连接(已经占用）；
     * 0x06：其他。
     */
    private RequestJtt809_0x1002_Result result;

    /**
     * 校验码
     */
    private int verifyCode;

    /**
     * 构造函数
     */
    public RequestJtt809_0x1002() {
        super(ConstantJtt809Util.UP_CONNECT_REP);
        this.msgBodyLength = 5;
    }

    public RequestJtt809_0x1002_Result getResult() {
        return result;
    }

    public void setResult(RequestJtt809_0x1002_Result result) {
        this.result = result;
    }

    public int getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(int verifyCode) {
        this.verifyCode = verifyCode;
    }

    @Override
    protected void encodeImpl(ByteBuf buf) {
        // 1 byte
        buf.writeByte(getResult().getRet());

        // 4 byte
        buf.writeInt(getVerifyCode());
    }
}
