package com.sjx.jtt809.server.codec;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.sjx.jtt809.server.pojo.BasePackage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.net.InetSocketAddress;

/**
 * jtt809编码类
 */
public class EncoderJtt809 extends MessageToByteEncoder<BasePackage> {

    private static final Log logger = LogFactory.get();

    @Override
    protected void encode(ChannelHandlerContext ctx, BasePackage basePackage, ByteBuf out) throws Exception {
        ByteBuf sendToMsg = basePackage.encode();
        logger.info("=====> 【上级平台|发送|{}】指令 = {} ， 数据 = {}", ((InetSocketAddress) ctx.channel().remoteAddress()).toString(), Integer.toHexString(basePackage.getMsgId()), ByteBufUtil.hexDump(sendToMsg));
        out.writeBytes(sendToMsg);
    }
}
