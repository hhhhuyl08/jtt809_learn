package com.sjx.util;

/**
 * 静态常量类
 */
public class ConstantUtil {

    /**
     * config配置文件名
     */
    public static final String PROPERTIES_CONFIG_NAME = "config.properties";

    /**
     * ip正则表达式
     */
    public static final String REGEX_IS_IP = "([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";

    /**
     * 服务地址
     */
    public static final String JTT809_NETTY_SERVER_IP = "jtt809.netty.server.ip";

    /**
     * 服务端口
     */
    public static final String JTT809_NETTY_SERVER_PORT = "jtt809.netty.server.port";

    /**
     * 下级平台平台账号登录帐号
     */
    public static final String JTT809_NETTY_SERVER_USERID = "jtt809.netty.server.userid";

    /**
     * 下级平台登录密码
     */
    public static final String JTT809_NETTY_SERVER_PASSWORD = "jtt809.netty.server.password";

    /**
     * 下级平台接入码，上级平台给下级平台分配唯一标识码。
     */
    public static final String JTT809_FACTORY_ACCESS_CODE = "jtt809.factory.access.code";

    /**
     * 收到x条下级平台发送的车辆定位信息后通知下级平台
     */
    public static final String JTT809_RECEIVE_GPS_OVERFLOW_NOTICE_COUNT = "jtt809.receive.gps.overflow.notice.count";
}
